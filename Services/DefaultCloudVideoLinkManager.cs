﻿using Datwendo.MediaServices.Models;
using Orchard;
using Orchard.ContentManagement;
using Orchard.Projections.Models;
using Orchard.Projections.Services;
using System.Collections.Generic;
using System.Linq;
using Orchard.Caching;
using Orchard.Logging;
using System.Web.Mvc;
using Orchard.FileSystems.AppData;
using Orchard.Environment.Configuration;

namespace Datwendo.MediaServices.Services
{
    public class DefaultCloudVideoLinkManager : ICloudVideoLinkManager
    {
        private readonly IWorkContextAccessor _workContextAccessor;
        private readonly IProjectionManager _projectionManager;
        private readonly IOrchardServices _orchardServices;

        public ILogger Logger { get; set; }

        public DefaultCloudVideoLinkManager(IProjectionManager projectionManager
            , IOrchardServices orchardServices
            , IWorkContextAccessor workContextAccessor
            , ICacheManager cacheManager
            , ISignals signals
            , UrlHelper UrlHlp
            , IAppDataFolder appDataFolder
            , ShellSettings shellSettings) {
            _projectionManager = projectionManager;
            _orchardServices = orchardServices;
            _workContextAccessor = workContextAccessor;
            Logger = NullLogger.Instance;
        }

        public List<QueryPart> GetCloudVideoQueries()
        {
            IEnumerable<QueryPart> queryParts = _orchardServices.ContentManager.Query<QueryPart, QueryPartRecord>("Query").List();

            List<QueryPart> CloudVideoLinkQueries = new List<QueryPart>();

            foreach (QueryPart part in queryParts)
            {
                ContentItem contentItem = _projectionManager.GetContentItems(part.Id).FirstOrDefault();
                if (contentItem == null)
                {
                    var isCloudVideoLink = part.FilterGroups.Where(fg => fg.Filters.Where(f => f.Type == "ContentTypes" && f.State.Contains("CloudVideoLink")).Any()).Any();
                    if (isCloudVideoLink)
                        CloudVideoLinkQueries.Add(part);
                    continue;
                }

                bool hasTitleParts = contentItem.TypeDefinition.Parts.Where(r => r.PartDefinition.Name == "TitlePart").Any();
                bool hasCloudVideoLinkPart = contentItem.TypeDefinition.Parts.Where(r => r.PartDefinition.Name == "CloudVideoLinkPart").Any();

                if (hasTitleParts && hasCloudVideoLinkPart)
                {
                    CloudVideoLinkQueries.Add(part);
                }
            }

            return CloudVideoLinkQueries;
        }

        public ContentItem GetCloudVideos(CloudVideoLinkWidgetPart part)
        {
            IEnumerable<ContentItem> contentItems = _projectionManager.GetContentItems(part.QueryId);

            ContentItem CloudVideoLink = contentItems.Where(c => c.Is<CloudVideoLinkPart>()).FirstOrDefault();

            return CloudVideoLink;
        }
    }
}
﻿using Datwendo.MediaServices.Models;
using Orchard;
using Orchard.ContentManagement;
using Orchard.Projections.Models;
using System.Collections.Generic;


namespace Datwendo.MediaServices.Services
{
    public interface ICloudVideoLinkManager : IDependency
    {
        ContentItem GetCloudVideos(CloudVideoLinkWidgetPart part);
        List<QueryPart> GetCloudVideoQueries();
        }
    }
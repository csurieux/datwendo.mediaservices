﻿/* PlayCloudVideoLink.js CS Datwendo 2015  v 1.0.0*/

function InitVideoPlayer(id,mediaUrl, isNativControls, isAutoplay, isControl, pxWidth, pxHeight, posterUrl) {
    var myPlayer = amp(id,  {
                                "nativeControlsForTouch": isNativControls,
                                "logo": { "enabled": false },
                                autoplay: isAutoplay,
                                controls: isControl,
                                width: pxWidth,
                                height: pxHeight,
                                poster: posterUrl
                            },
                            function () {
                                /*this.addEventListener('ended', function () {
                                    alert("Video ended");
                                })*/
                            });
    myPlayer.src([{
        src: mediaUrl,
        type: "application/vnd.ms-sstr+xml"
    }]);
}

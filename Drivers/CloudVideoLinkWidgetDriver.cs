﻿using Datwendo.MediaServices.Models;
using Datwendo.MediaServices.Services;
using Orchard;
using Orchard.ContentManagement;
using Orchard.ContentManagement.Drivers;
using Orchard.Localization;

namespace Datwendo.MediaServices.Drivers
{
    public class CloudVideoLinkWidgetDriver:ContentPartDriver<CloudVideoLinkWidgetPart>
    {
        private readonly IOrchardServices _orchardServices;
        private readonly ICloudVideoLinkManager _CloudVideoLinkService;

        public Localizer T { get; set; }

        public CloudVideoLinkWidgetDriver(IOrchardServices orchardServices, ICloudVideoLinkManager EditoService)
        {
            _orchardServices = orchardServices;
            _CloudVideoLinkService = EditoService;

            T = NullLocalizer.Instance;
        }

        protected override DriverResult Display(CloudVideoLinkWidgetPart part, string displayType, dynamic shapeHelper)
        {
            var contentItem = _CloudVideoLinkService.GetCloudVideos(part);
            var contentShape = _orchardServices.ContentManager.BuildDisplay(contentItem, "Detail");
            return ContentShape("Parts_CloudVideoLinkWidget",
            	() => shapeHelper.Parts_CloudVideoLinkWidget(
                	Content: contentShape 
                )
			);
        }

        protected override DriverResult Editor(CloudVideoLinkWidgetPart part, dynamic shapeHelper)
        {
            var model = new ViewModels.CloudVideoLinkWidgetPartWiewModel {
                Queries = _CloudVideoLinkService.GetCloudVideoQueries(),
                QueryId = part.QueryId
            };

            return ContentShape("Parts_CloudVideoLinkWidget_Edit",
            	() => shapeHelper.EditorTemplate(
            		TemplateName: "Parts/CloudVideoLinkWidget",
                	Model: model,
            		Prefix: Prefix
            	)
            );
        }

        protected override DriverResult Editor(CloudVideoLinkWidgetPart part, IUpdateModel updater, dynamic shapeHelper)
        {
            updater.TryUpdateModel(part, Prefix, null, new string[] { "Queries" });

            if (part.QueryId <= 0)
            {
                updater.AddModelError("QueryId", T("You must select a query."));
            }

            return Editor(part, shapeHelper);
        }
    }
}
﻿using Orchard.ContentManagement;
using Orchard.ContentManagement.Drivers;
using Orchard.ContentManagement.Handlers;
using Orchard.Localization;
using System;
using System.Web;
using Datwendo.MediaServices.Models;
using Orchard;

namespace Datwendo.MediaServices.Drivers
{
    public class CloudVideoLinkPartDriver : ContentPartDriver<CloudVideoLinkPart>
    {
        private const string TemplateName = "Parts/CloudVideoLinkPart";
        private IWorkContextAccessor _workContextAccessor;

        public Localizer T { get; set; }

        public CloudVideoLinkPartDriver(HttpContextBase context, IWorkContextAccessor workContextAccessor)
        {
            _workContextAccessor = workContextAccessor;
            T = NullLocalizer.Instance;
        }

        protected override string Prefix
        {
            get { return "CloudVideoLinkPart"; }
        }

        protected override DriverResult Display(CloudVideoLinkPart part, string displayType, dynamic shapeHelper)
        {
            return Combined(
                ContentShape("Parts_CloudVideoLinkPart",
                    () => shapeHelper.Parts_CloudVideoLinkPart(part)),
                ContentShape("Parts_CloudVideoLinkPart_Summary",
                    () => shapeHelper.Parts_CloudVideoLinkPart_Summary(part)),
                ContentShape("Parts_CloudVideoLinkPart_SummaryAdmin",
                    () => shapeHelper.Parts_CloudVideoLinkPart_SummaryAdmin(part))
                );
        }

        protected override DriverResult Editor(CloudVideoLinkPart part, dynamic shapeHelper)
        {
            return ContentShape("Parts_CloudVideoLinkPart_Edit",
                () => shapeHelper.EditorTemplate(TemplateName: TemplateName, Model: part, Prefix: Prefix));
        }

        protected override DriverResult Editor(CloudVideoLinkPart part, IUpdateModel updater, dynamic shapeHelper)
        {
            updater.TryUpdateModel(part, Prefix, null, null);

            return Editor(part, shapeHelper);
        }

        protected override void Importing(CloudVideoLinkPart part, ImportContentContext context)
        {
            base.Importing(part, context);

            throw new NotImplementedException();
        }
        protected override void Exporting(CloudVideoLinkPart part, ExportContentContext context)
        {
            base.Exporting(part, context);

            throw new NotImplementedException();
        }
    }
}
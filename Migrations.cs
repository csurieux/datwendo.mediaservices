﻿using Orchard.Data.Migration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Orchard.ContentManagement.MetaData;
using Orchard.Core.Contents.Extensions;
using Datwendo.MediaServices.Models;
using Orchard.ContentManagement.MetaData.Models;
using System.Data;

namespace Datwendo.MediaServices {
    public class Migrations : DataMigrationImpl {
        public int Create() {
            ContentDefinitionManager.AlterPartDefinition(typeof(CloudVideoLinkPart).Name,
                builder => builder
                    .Attachable()
                    .WithDescription("CloudVideo Link Part To add an Azure Media Service Video to your content"));

            ContentDefinitionManager.AlterTypeDefinition("CloudVideoOneLinkWidget",
                cfg => cfg
                    .WithPart("CloudVideoLinkPart")
                    .WithPart("CommonPart")
                    .WithPart("WidgetPart")
                    .WithSetting("Stereotype", "Widget")
                    );

            ContentDefinitionManager.AlterPartDefinition(typeof(CloudVideoLinkWidgetPart).Name,
                builder => builder
                    .Attachable()
                    .WithDescription("CloudVideo Link Widget Part"));

            ContentDefinitionManager.AlterTypeDefinition("CloudVideoLinkWidget",
                cfg => cfg
                    .WithPart("CloudVideoLinkWidgetPart")
                    .WithPart("CommonPart")
                    .WithPart("WidgetPart")
                    .WithSetting("Stereotype", "Widget")
                    );

            ContentDefinitionManager.AlterTypeDefinition("CloudVideoLink",
                cfg => cfg
                    .WithPart("CloudVideoLinkPart")
                    .WithPart("CommonPart")
                       .WithSetting("DateEditorSettings.ShowDateEditor", "True")
                    .WithPart("PublishLaterPart")
                    .WithPart("TitlePart")
                    .WithPart("AutoroutePart", builder => builder
                        .WithSetting("AutorouteSettings.AllowCustomPattern", "True")
                        .WithSetting("AutorouteSettings.AutomaticAdjustmentOnEdit", "False")
                        .WithSetting("AutorouteSettings.PatternDefinitions", "[{\"Name\":\"Title\",\"Pattern\":\"Video/{Content.Slug}\",\"Description\":\"my-CloudVideoLink\"}]")
                        .WithSetting("AutorouteSettings.DefaultPatternIndex", "0"))
                    .WithPart("IdentityPart")
                    .Creatable()
                    .Listable()
                    .Securable()
                );

            return 2;
        }

        public int UpdateFrom1()
        {
            ContentDefinitionManager.AlterPartDefinition(typeof(CloudVideoLinkPart).Name,
                builder => builder
                    .Attachable()
                    .WithDescription("CloudVideo Link Part To add an Azure Media Service Video to your content"));

            ContentDefinitionManager.AlterTypeDefinition("CloudVideoOneLinkWidget",
                cfg => cfg
                    .WithPart("CloudVideoLinkPart")
                    .WithPart("CommonPart")
                    .WithPart("WidgetPart")
                    .WithSetting("Stereotype", "Widget")
                    );
            return 2;
        }
    }
}
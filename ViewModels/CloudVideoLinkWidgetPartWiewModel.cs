﻿using System.Collections.Generic;
using Orchard.Projections.Models;

namespace Datwendo.MediaServices.ViewModels
{
    public class CloudVideoLinkWidgetPartWiewModel
    {
        public IEnumerable<QueryPart> Queries { get; set; }
        public int QueryId { get; set; }
    }
}
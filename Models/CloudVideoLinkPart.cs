﻿using Orchard.ContentManagement;
using Orchard.Core.Title.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datwendo.MediaServices.Models
{
    public class CloudVideoLinkPart : ContentPart
    {
        public string Title
        {
            get {  
                    var tt = this.ContentItem.As<TitlePart>();
                    return tt != null ? tt.Title: string.Empty;
            }            
        }

        public string MediaUrl
        {
            get { return this.Retrieve(x => x.MediaUrl); }
            set { this.Store(x => x.MediaUrl, value); }
        }

        public bool NativeControlsForTouch
        {
            get { return this.Retrieve(x => x.NativeControlsForTouch); }
            set { this.Store(x => x.NativeControlsForTouch, value); }
        }
        public bool Autoplay
        {
            get { return this.Retrieve(x => x.Autoplay); }
            set { this.Store(x => x.Autoplay, value); }
        }
        public bool Controls
        {
            get { return this.Retrieve(x => x.Controls); }
            set { this.Store(x => x.Controls, value); }
        }
        public int Width
        {
            get { return this.Retrieve(x => x.Width); }
            set { this.Store(x => x.Width, value); }
        }
        public int Height
        {
            get { return this.Retrieve(x => x.Height); }
            set { this.Store(x => x.Height, value); }
        }

        public string Poster
        {
            get { return this.Retrieve(x => x.Poster); }
            set { this.Store(x => x.Poster, value); }
        }

    }
}

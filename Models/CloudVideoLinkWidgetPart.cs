﻿using Orchard.ContentManagement;

namespace Datwendo.MediaServices.Models
{
    public class CloudVideoLinkWidgetPart : ContentPart
    {
        public int QueryId
        {
            get { return this.Retrieve(x => x.QueryId); }
            set { this.Store(x => x.QueryId, value); }
        }
    }
}
using Orchard.UI.Resources;

namespace Datwendo.MediaServices {
    public class ResourceManifest : IResourceManifestProvider {
        public void BuildManifests(ResourceManifestBuilder builder) {
            var manifest = builder.Add();
            manifest.DefineStyle("AzureMediaPlayer").SetCdn("//amp.azure.net/libs/amp/1.5.0/skins/amp-default/azuremediaplayer.min.css",null,true).SetVersion("1.5.0");
            manifest.DefineScript("AzureMediaPlayer").SetCdn("//amp.azure.net/libs/amp/1.5.0/azuremediaplayer.min.js",null,true).SetVersion("1.5.0");
            manifest.DefineScript("PlayCloudVideoLink").SetUrl("PlayCloudVideoLink.min.js", "PlayCloudVideoLink.js").SetDependencies("jQuery","AzureMediaPlayer").SetVersion("1.0.0");
        }
    }
}
